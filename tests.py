import unittest
import main.py

class TestFunc(unittest.TestCase):

    def test_add_numbers_positive(self):
        self.assertEqual(add_numbers(3, 5), 8)

    def test_add_numbers_negative(self):
        self.assertEqual(add_numbers(-2, 7), 5)

    def test_add_numbers_zero(self):
        self.assertEqual(add_numbers(0, 0), 0)

    def test_add_numbers_float(self):
        self.assertEqual(add_numbers(1.5, 2.5), 4.0)

if __name__ == '__main__':
    unittest.main()